@echo off

if exist "%~dp0Sources\*.dcu" del "%~dp0Sources\*.dcu"

if exist "%~dp0*.local" del "%~dp0*.local"
if exist "%~dp0*.identcache" del "%~dp0*.identcache"