unit FMain;

interface

uses
  SysUtils, Windows, Forms, Classes, Controls, StdCtrls,
  ComCtrls, Dialogs, Grids, Graphics, UModel;

type
  // ������� �����.
  TMainForm = class(TForm)
    FieldGrid: TStringGrid;
    OpenResDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    FieldSizeEdit: TEdit;
    CreateFieldButton: TButton;
    Label2: TLabel;
    SeedCountEdit: TEdit;
    SeedButton: TButton;
    Label3: TLabel;
    SeedXEdit: TEdit;
    SeedYEdit: TEdit;
    SeedXYButton: TButton;
    GroupBox2: TGroupBox;
    NormalizeCheckBox: TCheckBox;
    OpenResButton: TButton;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    StepCountEdit: TEdit;
    StartButton: TButton;
    Label5: TLabel;
    ResMinEdit: TEdit;
    Label6: TLabel;
    ResMaxEdit: TEdit;
    Label7: TLabel;
    GrowthConsumptionEdit: TEdit;
    Label8: TLabel;
    LivingConsumptionEdit: TEdit;
    JmpMemCheckBox: TCheckBox;
    NoTorCheckBox: TCheckBox;
    RemoveAllButton: TButton;
    StatusBar: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CreateFieldButtonClick(Sender: TObject);
    procedure SeedButtonClick(Sender: TObject);
    procedure SeedXYButtonClick(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure RemoveAllButtonClick(Sender: TObject);
    procedure OpenResButtonClick(Sender: TObject);
    procedure FieldGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FieldGridSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  private
    FModel: TModel;  // ������
  private
    // ��������� �������� ������ �������� ���������� �� ����� ����� �� �����. ����
    // ��������� �������� �����������, ������� ������� ��������� � ������ False.
    function UpdateLimits: Boolean;
    // ���������� �������� ������� �������� �� ����� ����� �� �����. ����
    // ��������� �������� �����������, ������� ������� ��������� � ������ False.
    function GetConsumption(var GrowthConsumption, LivingConsumption: Single): Boolean;
    // ��������� ������ ���������.
    procedure UpdateStatusBar;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.CreateFieldButtonClick(Sender: TObject);
var
  Size, E: Integer;
begin
  // ������������ ������, ��������� � ����
  // ����� "����������� ����" � ����� �����.
  Val(FieldSizeEdit.Text, Size, E);
  // ��������� ������������ �����.
  if (E <> 0) or (Size < 1) then begin
    Application.MessageBox('�������� ���� "����������� ����" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ������ ��� ������ ����� ������� ���� (� ������ ��� ������).
  CreateFieldButton.Caption := '�������� ����';

  // ���������������� ��������� ��. �����.
  Randomize;

  // ����� �������� ���� ���������
  // ������ ������ ����� ���������.
  SeedButton.Enabled := True;
  SeedXYButton.Enabled := True;
  OpenResButton.Enabled := True;
  RemoveAllButton.Enabled := True;
  // � ������ "������" ��������.
  StartButton.Enabled := False;

  // �������� ����������� ����� (����).
  FieldGrid.RowCount := Size;
  FieldGrid.ColCount := Size;
  FieldGrid.Visible := True;

  // ������� ���� ������� �������.
  FModel.CreateField(Size, Size);
  // ���������� ��������� ������.
  FModel.Reset;

  // �������� �����.
  //SeedButtonClick(Sender);

  // ��������� ���� (����).
  FieldGrid.Refresh;

  // ��������� ������ �������.
  StatusBar.Panels[0].Text := '���� �������';
  StatusBar.Panels[1].Text := Format('������ ����: %dx%d', [Size, Size]);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FieldGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Color: Byte;
begin
  // ���������, ���� �� � ������ ����.
  if FModel.Field[Acol, Arow].Exists then
    // ���� ����, �� ����� ������� ����.
    FieldGrid.Canvas.Brush.Color := clRed
  else begin
    // ���� � ������ ��� �����, �� ��������� �������� ������������� ����� (�� 0
    // �� 195). ��� ������ �������� � ������, ��� ���� �������� �������������.
    Color := Round(195 * FModel.Field[Acol, Arow].Source);
    // ���� � ������ �������� ��� (������������� �����
    // ����� 0), �� �������� ��� ������ ����� ����.
    if Color = 0 then
      FieldGrid.Canvas.Brush.Color := clWhite
    else
      // ���� ������� ����, �� �������� ��� ������ ������
      // ����: ��� ������ ��������, ��� ���� ����� �� ����.
      FieldGrid.Canvas.Brush.Color := RGB(60 + Color, 60 + Color, 0);
  end;
  // ����������� ������ ��������� ������.
  FieldGrid.Canvas.FillRect(Rect);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FieldGridSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
var
  Info: String;
  Resources: Single;
begin
  Info := '�����';
  Resources := 0.0;
  // ���� ���������� ���������, ��������� ���������� �������� � ������.
  if (ACol >= 0) and (ACol < FModel.Field.Width) and (ARow >= 0) and (ARow < FModel.Field.Height) then begin
    Resources := FModel.Field[ACol, ARow].Source;
    if FModel.Field[ACol, ARow].Exists then
      Info := Format('����: ����-� ����� %d', [Ord(FModel.Field[ACol, ARow].Direction[1])]);
  end;
  // ������� � ������ ������� ���������� ������, ���-�� �������� � ���� �� � ��� ����.
  StatusBar.Panels[2].Text := Format('������ [%d,%d]: ���. %.4f; %s',
    [ACol + 1, ARow + 1, Resources, Info]);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FormCreate(Sender: TObject);
begin
  // ������� ������ ������.
  FModel := TModel.Create;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.FormDestroy(Sender: TObject);
begin
  // ���������� ������ ������.
  FreeAndNil(FModel);
end;

//----------------------------------------------------------------------------------------------------------------------
function TMainForm.GetConsumption(var GrowthConsumption, LivingConsumption: Single): Boolean;
var
  E: Integer;
begin
  Result := False;
  // ��������� ��������, ��������� � ���� "����. ���. �� ����". ����
  // � ��� ���� ������, ������� ��������� � ���������� � �������.
  Val(GrowthConsumptionEdit.Text, GrowthConsumption, E);
  if (E <> 0) or (GrowthConsumption <= 0.0) then begin
    Application.MessageBox('�������� ���� "����. ���. �� ����" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;
  // ��������� ��������, ��������� � ���� "����. ���. �� �����". ����
  // � ��� ���� ������, ������� ��������� � ���������� � �������.
  Val(LivingConsumptionEdit.Text, LivingConsumption, E);
  if (E <> 0) or (LivingConsumption <= 0.0) then begin
    Application.MessageBox('�������� ���� "����. ���. �� �����" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;
  // ��� �������.
  Result := True;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.OpenResButtonClick(Sender: TObject);
var
  Normalize, Loaded: Boolean;
begin
  // ��������� ���� ������ �����.
  OpenResDialog.Title := '�������� ����� �������� �� �����...';
  // ��������� ���� ������ �����.
  if OpenResDialog.Execute then begin
    Normalize := NormalizeCheckBox.Checked;
    Loaded := FModel.Field.LoadResourcesFromFile(OpenResDialog.FileName, Normalize);
    // ��������� ����.
    FieldGrid.Refresh;
    // ��������� ������ �������.
    if Loaded then
      StatusBar.Panels[0].Text := '������� ���������'
    else
      StatusBar.Panels[0].Text := '������ �������� ��������';
    StatusBar.Panels[2].Text := '';
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.RemoveAllButtonClick(Sender: TObject);
var
  X, Y: Integer;
begin
  // ��������� ������ "������".
  StartButton.Enabled := False;

  // ������� � ���� ��� �����.
  for X := 0 to FModel.Field.Width - 1 do begin
    for Y := 0 to FModel.Field.Height - 1 do begin
      FModel.Field.Kill(X, Y);
      FModel.Field[X, Y].Age := 0;
      FModel.Field[X, Y].StepBirth := 0;
    end;
  end;
  // ��������� ������.
  FModel.Reset;

  // ������ �������� �����.
  //SeedButtonClick(Sender);

  // ��������� ���� (����).
  FieldGrid.Refresh;

  // ��������� ������ �������.
  UpdateStatusBar;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.SeedButtonClick(Sender: TObject);
var
  Count, E: Integer;
begin
  // ������������ ������, ��������� � ����
  // ����� "����� ������" � ����� �����.
  Val(SeedCountEdit.Text, Count, E);
  // ��������� ������������ �����.
  if (E <> 0) or (Count < 0) or (Count >= FModel.Field.Width * FModel.Field.Height) then begin
    Application.MessageBox('�������� ���� "����� ������" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ��������� �������� ������ ��������.
  if not UpdateLimits then Exit;

  // �������� Count (��� ������) ������. ���� ����� ������� ���� ��
  // 1 ������, ��������� ������ "������". ����� ������� ���������.
  if FModel.Seed(Count) then
    StartButton.Enabled := True
  else
    Application.MessageBox('����� �� ���� �������: �� ���� ��� ���������� ������',
     '����� ������', MB_ICONINFORMATION or MB_OK);

  // ��������� ���� (����).
  FieldGrid.Refresh;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.SeedXYButtonClick(Sender: TObject);
var
  X, Y, E1, E2: Integer;
begin
  // ������������ ������, ��������� � ����
  // ����� "���������� ������" � ����� �����.
  Val(SeedXEdit.Text, X, E1);
  Val(SeedYEdit.Text, Y, E2);
  // ��������� ������������ ���������.
  if (E1 <> 0) or (E2 <> 0) or (X < 0) or (Y < 0) or (X >= FModel.Field.Width) or (Y >= FModel.Field.Height) then begin
    Application.MessageBox('�������� � ����� "���������� ������" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ��������� �������� ������ ��������.
  if not UpdateLimits then Exit;

  // �������� ������ X,Y. ���� ������ �����
  // �������, �� ��������� ������ "������".
  if FModel.Seed(X, Y) then StartButton.Enabled := True;

  // ��������� ���� (����).
  FieldGrid.Refresh;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.StartButtonClick(Sender: TObject);
var
  Count, E, I: Integer;
  GrowthConsumption, LivingConsumption: Single;
begin
  // ������������ ��������� � ���� �����
  // "���������� �����" ������ � ����� �����.
  Val(StepCountEdit.Text, Count, E);
  // ��������� ������������ �����.
  if (E <> 0) or (Count < 1) then begin
    Application.MessageBox('�������� ���� "���������� �����" �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
    Exit;
  end;

  // ��������� �������� ������ ��������.
  if not UpdateLimits then Exit;
  // �������� �������� ������� ��������.
  if not GetConsumption(GrowthConsumption, LivingConsumption) then Exit;
  // ��������� ���� ������� ������ ��� ������.
  FModel.ClearMemAfterJump := not JmpMemCheckBox.Checked;

  // ��������� ������ "������".
  StartButton.Enabled := False;

  // ���������� ��������� ���������� �����.
  for I := 1 to Count do begin
    // ���������� ��������� ���.
    FModel.DoStep(GrowthConsumption, LivingConsumption, NoTorCheckBox.Checked);
    // �������������� ���� � ����.
    FieldGrid.Refresh;
    // ��������� ������ �������.
    UpdateStatusBar;
    // ������������ �����������
    // ������� � ������� ����.
    Application.ProcessMessages;
  end;

  // ����� ��������� ������.
  StartButton.Enabled := True;
end;

//----------------------------------------------------------------------------------------------------------------------
function TMainForm.UpdateLimits: Boolean;
begin
  // ��������� �������� ������ ��������.
  Result := FModel.Limits.SetLimits(ResMinEdit.Text, ResMaxEdit.Text);
  // ������� ��������� � ������ �������������� ������.
  if not Result then begin
    Application.MessageBox('�������� ������ �������� �����������',
      '������ � ���������� �����', MB_ICONERROR or MB_OK);
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TMainForm.UpdateStatusBar;
begin
  StatusBar.Panels[0].Text := '��� �������������: ' + IntToStr(FModel.Step);
  StatusBar.Panels[2].Text := '';
end;

end.
