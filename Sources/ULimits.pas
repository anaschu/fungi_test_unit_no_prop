unit ULimits;

interface

type
  // ������� ��������.
  TLimits = record
    StarvationDeath: Single;  // �������� �������� � ������, ���� �������� ��������� �������� ������
    OvereatingToxic: Single;  // �������� �������� � ������, ���� �������� ���� ����������
  public
    // ��������� ����� ������. ���� �������� ���������, ������� ��������
    // �� � ������ True. ���� �������� �����������, ������� ������ False.
    function SetLimits(const StarvationDeath, OvereatingToxic: String): Boolean;
  end;

implementation

//----------------------------------------------------------------------------------------------------------------------
function TLimits.SetLimits(const StarvationDeath, OvereatingToxic: String): Boolean;
var
  StarvationValue, OvereatingValue: Single;
  E: Integer;
begin
  Result := False;
  // ������������ �������� Starvation.
  Val(StarvationDeath, StarvationValue, E);
  // ��������� ������������ �������� Starvation.
  if (E <> 0) or (StarvationValue < 0.0) then Exit;
  // ������������ �������� Overeating.
  Val(OvereatingToxic, OvereatingValue, E);
  // ��������� ������������ �������� Overeating.
  if (E <> 0) or (OvereatingValue < 0.0) or (OvereatingValue <= StarvationValue) then Exit;
  // ��������� ��� ����� �������� ������.
  Self.StarvationDeath := StarvationValue;
  Self.OvereatingToxic := OvereatingValue;
  // ��� �������.
  Result := True;
end;

end.
