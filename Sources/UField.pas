unit UField;

interface

uses
  SysUtils, Types, UCoord;

type
  // ������ ����.
  TCell = class
  public
    Exists: Boolean;                       // ���� �� ���� � ������
    Age: Integer;                          // ������� �����
    Source: Single;                        // ������ � ������
    Direction: array[1..2] of TDirection;  // ����������� ����� �����
    DickLength: Integer;                   // ���-�� �����, � ������� ������� ���� ��� � ����� �����������
    StepBirth: Integer;                    // ���, �� ������� ������ ���� �������
  end;

  // ����.
  TField = class
  private
    FWidth: Integer;         // ������ ���� � �������
    FHeight: Integer;        // ������ ���� � �������
    FCells: array of TCell;  // ������ ������ ����
  private
    // ����� ������� ��� �������� Cell.
    function GetCell(X, Y: Integer): TCell;
    // ����������� ������ ����� Data ([0.0 .. 1.0]).
    procedure NormalizeArray(var Data: array of Single);
    // ��������� � ������ Data ����� �� ����� FilePath. � ��������� Size ������������ ����������� �������.
    function LoadArrayFromFile(const FilePath: String; var Size: Integer; var Data: TSingleDynArray): Boolean;
  public
    // �����������.
    constructor Create(Width, Height: Integer);
    // ����������.
    destructor Destroy; override;
    // ���������� ����������������� ����������.
    function Tor(X, Y: Integer): TCoord; overload;
    function Tor(const C: TCoord): TCoord; overload;
    // ���������� True, ���� ���������� ��������� � �������� ����.
    function IsInside(X, Y: Integer): Boolean;
    // ������� ���� � ��������� ������, ���� �� ��� ����.
    procedure Kill(X, Y: Integer);
    // �������� ����� �������� �� ���������� �����. ���� ��� �������� ����� ��������� ������,
    // ������� ������ False. �������� Normalize ����������, ����� �� �������������� ��������.
    function LoadResourcesFromFile(const FilePath: String; Normalize: Boolean): Boolean;
  public
    // ������ � ������ ���� � �������.
    property Width: Integer read FWidth;
    property Height: Integer read FHeight;
    // ������ ���� (������� X, Y ������ ���������� �� 0).
    property Cell[X, Y: Integer]: TCell read GetCell; default;
  end;

implementation

//----------------------------------------------------------------------------------------------------------------------
constructor TField.Create(Width, Height: Integer);
var
  Count, I: Integer;
begin
  // ���������� ������ � ������ ����.
  FWidth := Width; FHeight := Height;
  // ������������ �������� ������ � ������.
  if (FWidth <= 0) or (FHeight <= 0) then begin
    FWidth := 0;
    FHeight := 0;
  end;
  // ����� ���������� ������.
  Count := FWidth * FHeight;
  // ������ ������ ������� FCells.
  SetLength(FCells, Count);
  // ������� ������� ������.
  for I := 0 to Count - 1 do
    FCells[I] := TCell.Create;
end;

//----------------------------------------------------------------------------------------------------------------------
destructor TField.Destroy;
var
  I: Integer;
begin
  // ���������� ������� ������.
  for I := 0 to High(FCells) do
    FreeAndNil(FCells[I]);
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.GetCell(X, Y: Integer): TCell;
begin
  // ��������� ������������ ���������.
  if (X < 0) or (X >= FWidth) or (Y < 0) or (Y >= FHeight) then
    raise Exception.Create('������������ ���������� ������');
  // ���������� ������ ������.
  Result := FCells[X + Y * FWidth];
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.IsInside(X, Y: Integer): Boolean;
begin
  Result := (X >= 0) and (X < FWidth) and (Y >= 0) and (Y < FHeight);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TField.Kill(X, Y: Integer);
var
  Cell: TCell;
begin
  // �������� ������ ������.
  Cell := GetCell(X, Y);
  // ������� ������.
  Cell.Exists := False;
  Cell.Direction[1] := dNone;
  Cell.Direction[2] := dNone;
  Cell.DickLength := 0;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.LoadArrayFromFile(const FilePath: String; var Size: Integer; var Data: TSingleDynArray): Boolean;
var
  F: TextFile;
  X, Y, K: Integer;
  FS: TFormatSettings;
  Delim, DS: Char;
  S, S1: String;
begin
  Data := nil;
  Result := False;
  // ���������� ��������� ����������� ������� �����.
  GetLocaleFormatSettings(0, FS);
  DS := FS.DecimalSeparator;
  try
    // ��������� ����.
    AssignFile(F, FilePath);
    Reset(F);
    try
      // ������ ����� ������ ����� ����� - ����������� ����.
      Readln(F, Size);
      // ��������� ��������.
      if Size <= 0 then Exit;
      // �������� ������ ��� ������.
      SetLength(Data, Size * Size);
      // ��������� ��������� ������ �� �����.
      for Y := 0 to Size - 1 do begin
        // ������ ������.
        Readln(F, S);
        // ���������� ����������� �����.
        if Pos(';', S) > 0 then Delim := ';' else Delim := ',';
        // ��������� ������: � ��� ������ ���� Size �����, ����������� Delim.
        for X := 0 to Size - 1 do begin
          // ������� ������� ��������� �������.
          K := Pos(Delim, S);
          // ���� ����������� ���, ����� ��� ������.
          if K = 0 then K := Length(S) + 1;
          // �������� ����������� ������� �����.
          S1 := StringReplace(Copy(S, 1, K - 1), ',', DS, [rfReplaceAll]);
          S1 := StringReplace(S1, '.', DS, [rfReplaceAll]);
          // ��������� ������ � ����� � ��������� � Data.
          Data[X + Y * Size] := StrToFloat(S1);
          // ������� ����������� ����� �� ������.
          S := Copy(S, K + 1, MaxInt);
        end;
      end;
      // ������ ������� ���������.
      Result := True;
    finally
      // ��������� ����.
      CloseFile(F);
    end;
  except
    // ������ ���������� ������. ������� ������ False.
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.LoadResourcesFromFile(const FilePath: String; Normalize: Boolean): Boolean;
var
  Data: TSingleDynArray;
  Size, X, Y: Integer;
begin
  // ��������� ������ ����� �� �����.
  Result := LoadArrayFromFile(FilePath, Size, Data);
  // ����������� ������ � ������ ������.
  if not Result then Exit;
  // ����������� ��������, ���� �����.
  if Normalize then NormalizeArray(Data);
  // �������� ������� �������� �� ����.
  for Y := 0 to FHeight - 1 do begin
    for X := 0 to FWidth - 1 do begin
      if (X < Size) and (Y < Size) then
        FCells[X + Y * FWidth].Source := Data[X + Y * Size]
      else
        FCells[X + Y * FWidth].Source := 0.0;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TField.NormalizeArray(var Data: array of Single);
var
  RMin, RMax: Single;
  I: Integer;
begin
  if Length(Data) = 0 then Exit;
  // ������� ����. � ���. ��������.
  RMin := Data[0]; RMax := RMin;
  for I := 1 to High(Data) do begin
    if Data[I] < RMin then RMin := Data[I];
    if Data[I] > RMax then RMax := Data[I];
  end;
  // ������ ����������� ��������.
  for I := 0 to High(Data) do begin
    if RMax - RMin > 0 then
      Data[I] := (Data[I] - RMin) / (RMax - RMin)
    else
      Data[I] := 0.5;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.Tor(X, Y: Integer): TCoord;
begin
  // �������� ����������.
  Result.X := X; Result.Y := Y;
  // ���� ���������� X ������� ��
  // ������� ����, ������������ ��.
  if Result.X < 0 then
    Inc(Result.X, FWidth)
  else if Result.X >= FWidth then
    Dec(Result.X, FWidth);
  // ���� ���������� Y ������� ��
  // ������� ����, ������������ ��.
  if Result.Y < 0 then
    Inc(Result.Y, FHeight)
  else if Result.Y >= FHeight then
    Dec(Result.Y, FHeight);
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.Tor(const C: TCoord): TCoord;
begin
  Result := Tor(C.X, C.Y);
end;

end.
