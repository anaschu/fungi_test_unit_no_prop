unit UModel;

interface

uses
  SysUtils, ULimits, UCoord, UField;

type
  // ������.
  TModel = class
  private
    FField: TField;               // ����
    FLimits: TLimits;             // ������� ��������
    FStep: Integer;               // ������� ��� ������������� (�� 0)
    FClearMemAfterJump: Boolean;  // ���� True, �� ������ � ����������� ����� ��������� ����� ������
  private
    // �������� ������ (X,Y). �������� Direction - ����������� ����� ��� ������.
    procedure OccupyCell(X, Y: Integer; Direction: TDirection);
    // �������� �� ������ �������, ����������� ������ �� ����� �� ���� ���. ���� ����������
    // �������� � ������ ����� ������ ��� ����� �������� FLimits.StarvationDeath, �� �������
    // ������ False. ���� �������� ������ (���������� ��� �����), �� ������� ������ True.
    function ConsumeLivingResources(X, Y: Integer; Cons: Single): Boolean;
    // �������� �� ������ �������, ����������� �� ���� ����� � ��� ������.
    procedure ConsumeGrowthResources(X, Y: Integer; Cons: Single);
    // ��������� �������� ������ �� ���������� �������� � ���������� �����������
    // �����. ���� ���� ����������, �� ������� ������ �������� dNone.
    function TestResources(X, Y: integer; var Prob: Single; NoTor: Boolean): TDirection;
    // ������� ����� ������ (������ ��� ������ 1-�� ����).
    procedure FirstGrow(X, Y: Integer; NoTor: Boolean);
    // ������� ����� ������ (��������).
    procedure Grow(X, Y: Integer; GrowthConsumption: Single; NoTor: Boolean);
  public
    // �����������.
    constructor Create;
    // ����������.
    destructor Destroy; override;
    // ������� ���� ���������� �������.
    procedure CreateField(Width, Height: Integer);
    // ���������� ������ � �������� ���������.
    procedure Reset;
    // �������� Count ������ � ��������� ������ ����. ����������
    // True, ���� ���� �� ���� ��������� ������ ���� �������.
    function Seed(Count: Integer): Boolean; overload;
    // �������� ��������� ������. ���������� True, ���� ������
    // ���� ��������, ��� False, ���� ������ ��� ���� ������.
    function Seed(X, Y: Integer): Boolean; overload;
    // ��������� ���� ��� �������������.
    procedure DoStep(GrowthConsumption, LivingConsumption: Single; NoTor: Boolean);
  public
    // ����.
    property Field: TField read FField;
    // ������� ��������.
    property Limits: TLimits read FLimits;
    // ������� ��� ������������� (�� 0).
    property Step: Integer read FStep;
    // True, ���� ������ � ����������� ����� ��������� ����� ������.
    property ClearMemAfterJump: Boolean read FClearMemAfterJump write FClearMemAfterJump;
  end;

implementation

//----------------------------------------------------------------------------------------------------------------------
constructor TModel.Create;
begin
  // ������� ��������� ���� 5x5.
  FField := TField.Create(5, 5);
  // ���������� ���������.
  Reset;
end;

//----------------------------------------------------------------------------------------------------------------------
destructor TModel.Destroy;
begin
  // ���������� ����.
  FField.Free;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.ConsumeGrowthResources(X, Y: Integer; Cons: Single);
var
  Resources: Single;
begin
  // ���������, ��� � ��������� ������ ���� ����.
  if FField[X, Y].Exists then begin
    // �������� ������� �� ������.
    Resources := FField[X, Y].Source - Cons;
    if Resources < 0.0 then Resources := 0.0;
    // ���������� ��������� ������� � ������.
    FField[X, Y].Source := Resources;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.ConsumeLivingResources(X, Y: Integer; Cons: Single): Boolean;
var
  Resources: Single;
begin
  Result := False;
  // ���������, ��� � ��������� ������ ���� ����.
  if FField[X, Y].Exists then begin
    // �������� �������.
    Resources := FField[X, Y].Source - Cons;
    if Resources < 0.0 then Resources := 0.0;
    // ���������� ��������� ������� � ������.
    FField[X, Y].Source := Resources;
    // ���������� True, ���� �������� ������ ��������.
    Result := Resources > FLimits.StarvationDeath;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.CreateField(Width, Height: Integer);
begin
  // ���������� ����.
  FreeAndNil(FField);
  // � ������� ����� ������� �������.
  FField := TField.Create(Width, Height);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.DoStep(GrowthConsumption, LivingConsumption: Single; NoTor: Boolean);
var
  X, Y: Integer;
  IsAlive: Boolean;
begin
  // ������� ��� ������ ����.
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      // ���������, ��� � ������ ���� ����.
      if FField[X, Y].Exists then begin
        // ��� ������ ������� ���� �������� ������� FirstGrow,
        // ��� ��������� �������� ������� Grow.
        if FStep = 0 then
          FirstGrow(X, Y, NoTor)
        else
          Grow(X, Y, GrowthConsumption, NoTor);
        // ����������� ������� �����.
        Inc(FField[X, Y].Age);
      end;
    end;
  end;

  // ������� ��� ������ ����.
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      // ���������, ��� � ������ ���� ����.
      if FField[X, Y].Exists then begin
        // �������� ������� �� �����.
        IsAlive := ConsumeLivingResources(X, Y, LivingConsumption);
        // ���� ��� �� ���, �� ������� ���� ��� ������ �
        // ������� � ������ �����������, �� ���� ������ ���������.
        if not IsAlive and (FField[X, Y].StepBirth <> FStep) then
          FField.Kill(X, Y);
      end;
    end;
  end;

  // ����������� ������� �����.
  Inc(FStep);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.FirstGrow(X, Y: Integer; NoTor: Boolean);
var
  Dir, D: TDirection;  // ����������� �����
  Prob: Single;        // �����������
  K: Integer;
  C: TCoord;
begin
  // ������ �� ������ �� ���� ��������.
  if FField[X, Y].StepBirth = FStep then Exit;

  // �������� ����������� ����� �� ��������.
  Dir := TestResources(X, Y, Prob, NoTor);
  // ���� ����������� �� ����������, �� �� �� ������.
  if Dir = dNone then Exit;

  // ��������� ���������� ������.
  C := Coord(X, Y) + Coord(Dir);
  if ((not NoTor) or (FField.IsInside(C.X, C.Y))) then begin
    C := FField.Tor(C);
    // ������ � ��� ������.
    OccupyCell(C.X, C.Y, Dir);
  end;

  // ���� �������� �����, �������� ������ ����������� �����.
  if FField[X, Y].Source > FLimits.OvereatingToxic then begin
    // ������ 8 ������� ������ ������.
    for K := 1 to 8 do begin
      // �������� �������� �����������.
      D := TDirection(Random(Ord(High(TDirection)) + 1));
      // ���������, ��� ��� ����������� ���������� �� 1-��.
      if D <> Dir then begin
        // ������� ����������.
        C := Coord(X, Y) + Coord(D);
        if ((not NoTor) or (FField.IsInside(C.X, C.Y))) then begin
          C := FField.Tor(C);
          // ���������, ��� � ���� ������ ���������� ��������.
          if (FField[C.X, C.Y].Source > FLimits.StarvationDeath) then begin
            // ������ � ��� ������.
            OccupyCell(C.X, C.Y, D);
            // ����������� �������.
            Break;
          end;
        end;
      end;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.Grow(X, Y: Integer; GrowthConsumption: Single; NoTor: Boolean);
const
  // ����������� ����� � ���� � �� �� �������.
  Probability: array[0..3] of Single = (0.426, 0.625, 0.899, 0.976);
var
  Jumped, Found: Boolean;
  RDir, Dir: TDirection;
  DirCount, K: Integer;
  Prob: Single;
  C: TCoord;
begin
  // ������ �� ������ �� ���� ��������.
  if FField[X, Y].StepBirth = FStep then Exit;

  DirCount := 1;
  // ���� �������� ���������� ��� ���������, �� ����� ����� � ���� ������������.
  if FField[X, Y].Source > FLimits.OvereatingToxic then
    DirCount := 2;

  Jumped := False;
  // ���� �����: 1 ��� 2 ��������.
  for K := 1 to DirCount do begin
    Prob := 0.0;
    // ������� ����������� �����.
    RDir := TestResources(X, Y, Prob, NoTor);
    // �������������� ����������.
    Dir := dNone;
    Found := False;
    C.X := 0; C.Y := 0;
    // ���� ����������� ����� ���������� � 1) �� ���� ����� 4 � ����� ������ ������ � ���� �����������,
    // ���� 2) ����������� ����������� ����� ���������� ��� ����������� ����� � ���� �� �����������.
    if (FField[X, Y].Direction[K] <> dNone) and ((FField[X, Y].DickLength >= 4) or
      // ����������� ������� ������ ����� ������ ����� ���� ����.
      ((Prob < Probability[FField[X, Y].DickLength]) and (Random < Probability[FField[X, Y].DickLength]))) then
    begin
      // ������� ���������� ������.
      Dir := FField[X, Y].Direction[K];
      C := Coord(X, Y) + Coord(Dir);
      if ((not NoTor) or (FField.IsInside(C.X, C.Y))) then begin
        C := FField.Tor(C);
        // ���� ��� ������ ��������.
        Found := not FField[C.X, C.Y].Exists;
        // ����� �������. ���� ������ � ������������ ��������� �� �������, ��... ����
        // DickLength ����� 3, �������� ����������� ����� ������ � ��� �� �����������.
        if Found and ((FField[C.X, C.Y].Source <= FLimits.StarvationDeath) and (FField[X, Y].DickLength > 3)) then begin
          // ������ �������� ������ � 4 ������������ (����� ����������).
          if Dir in [dLeft, dUp, dRight, dDown] then begin
            C := Coord(X, Y) + 2 * Coord(Dir);
            if ((not NoTor) or (FField.IsInside(C.X, C.Y))) then begin
              C := FField.Tor(C);
              // ���� ��� ������ ��������.
              Found := not FField[C.X, C.Y].Exists;
              // ���� ������ ��������, �������.
              Jumped := Found;
            end;
          end;
        end;
      end;
    end;
    // ���� �� �� ����� ������, � ������� ����� �����...
    if not Found then begin
      Dir := RDir;
      // ������� ���������� ������.
      C := Coord(X, Y) + Coord(Dir);
      if ((not NoTor) or (FField.IsInside(C.X, C.Y))) then begin
        C := FField.Tor(C);
        // ���� ��� ������ ��������.
        Found := not FField[C.X, C.Y].Exists;
      end;
    end;
    // ���� ����� ������ � �������� > ������������.
    if Found and (FField[C.X, C.Y].Source > FLimits.StarvationDeath) then begin
      // �������� ������.
      FField[C.X, C.Y].Exists := True;
      FField[C.X, C.Y].StepBirth := FStep;
      FField[C.X, C.Y].Age := 0;

      // �������� ������� �� ����.
      ConsumeGrowthResources(C.X, C.Y, GrowthConsumption);
      // ������ ����������� ������ �������. �����, ���� �� �������, � ������ ��������...
      if (FField[C.X, C.Y].Source < FLimits.OvereatingToxic) and (not Jumped or not FClearMemAfterJump) then begin
        FField[C.X, C.Y].Direction[K] := Dir;
        // ���� � ����� �� ������ � ��� �� �����������, ��...
        if FField[X, Y].Direction[K] = Dir then
          // ����������� ���������� ������������.
          FField[C.X, C.Y].DickLength := 1 + FField[X, Y].DickLength
        else
          // ���� ����������� ����������, �������� ��� �������.
          FField[C.X, C.Y].DickLength := 1;
      end else begin
        // ���� �������� ����� ����� ��� ��� ������ � ������ ���������.
        FField[C.X, C.Y].Direction[K] := dNone;
        FField[C.X, C.Y].DickLength := 0;
      end;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.OccupyCell(X, Y: Integer; Direction: TDirection);
begin
  // �������� ������.
  FField[X, Y].Exists := True;
  FField[X, Y].StepBirth := FStep;
  FField[X, Y].Age := 0;
  // ���������� ����������� �����.
  FField[X, Y].Direction[1] := Direction;
  FField[X, Y].Direction[2] := dNone;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.Reset;
begin
  // �������� �� ���������.
  FLimits.StarvationDeath := 0.2;
  FLimits.OvereatingToxic := 0.6;
  // ���������� ������� �����.
  FStep := 0;
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.Seed(Count: Integer): Boolean;
var
  Cells: array of TCoord;
  X, Y, L, I, J: Integer;
  C: TCoord;
begin
  // ������� ��� ���������� ������
  // � ��������� �� � ������ Cells.
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      // ������ ������ ���� ��������� � � ��� ������ ���� ���������� ��������.
      if not FField[X, Y].Exists and (FField[X, Y].Source > FLimits.StarvationDeath) then begin
        L := Length(Cells);
        SetLength(Cells, L + 1);
        Cells[L] := Coord(X, Y);
      end;
    end;
  end;

  // ������������ ������ Cells.
  for I := 0 to High(Cells) - 2 do begin
    J := I + 1 + Random(High(Cells) - I);
    C := Cells[I];
    Cells[I] := Cells[J];
    Cells[J] := C;
  end;

  // ���� ���������� ������ ������������, �� ������������
  // �������� ���������� ���������� ������ Count.
  if Count > Length(Cells) then
    Count := Length(Cells);

  // �������� ����� � ��������� ������.
  for I := 0 to Count - 1 do
    Seed(Cells[I].X, Cells[I].Y);

  // ������� ������ True, ���� ����
  // �� 1 ������ ����� �������.
  Result := (Count > 0);
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.Seed(X, Y: Integer): Boolean;
begin
  Result := False;
  // ���������, ��� ������ ��������� ������ ���� (� �� �� ��� ���������).
  if FField.IsInside(X, Y) then begin
    // �������� ������: ��� ���������� ������, ��� ������ ������ ��������� ��������� ��
    // ���������� ���� �������������, � �� ������� ��� ���� ������ ���� ����� 1 ����.
    with FField[X, Y] do begin
      Exists := True;
      Direction[1] := dNone;
      Direction[2] := dNone;
      DickLength := 0;
      StepBirth := FStep - 1;
      Age := 1;
    end;
    Result := True;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.TestResources(X, Y: Integer; var Prob: Single; NoTor: Boolean): TDirection;
var
  Res: Single;             // ������ �������� �������� � ������� Cells
  Delta: Single;           // ������� � ��������, ��� ������� ������ ��������� "�����������" ��������
  Cells: array of TCoord;  // ���������� ������ � ��������� ����������� ��������
  L, I, J: Integer;
  C: TCoord;
begin
  Res := 0.0;
  Delta := 0.01;
  // ��������� �������� ������.
  for I := X - 1 to X + 1 do begin
    for J := Y - 1 to Y + 1 do begin
      // ���������� ���� ������ [X, Y].
      if ((I <> X) or (J <> Y)) and ((not NoTor) or (FField.IsInside(I, J))) then begin
        // �������� ���������� ������.
        C := FField.Tor(I, J);
        // ���� ������ �������� � � ��� ���������� ��������, ��� �������� ��� �����.
        if not FField[C.X, C.Y].Exists and (FField[C.X, C.Y].Source > FLimits.StarvationDeath) then begin
          // ���������� �������� �������� ����� �������� � ��� ���������� �������?
          if Abs(FField[C.X, C.Y].Source - Res) < Delta then begin
            // ��������� ������ � ������.
            L := Length(Cells);
            SetLength(Cells, L + 1);
            Cells[L] := Coord(I, J);
          end
          // ��� � ���� ������ ������ ��������, ��� � ��� ���������?
          else if FField[C.X, C.Y].Source > Res then begin
            // ��, �������� ������, ���������� ����� ��������.
            Res := FField[C.X, C.Y].Source;
            // ������� ��� ��������� ������ � �������� ��� ������ � ������.
            SetLength(Cells, 1);
            Cells[0] := Coord(I, J);
          end;
        end;
      end;
    end;
  end;

  Prob := 0.0;
  Result := dNone;
  // ���� ��������� (��������� ������)?
  if Length(Cells) > 0 then begin
    // �����������.
    Prob := 0.225;
    if Res > FLimits.OvereatingToxic then Prob := 0.625;
    // �������� �������� ���� ������ �� �������.
    L := Random(Length(Cells));
    // ������� �������� ��������� � �����������.
    C := Cells[L] - Coord(X, Y);
    Result := C.Direction;
  end;
end;

end.
