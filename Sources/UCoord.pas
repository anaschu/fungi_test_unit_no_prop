unit UCoord;

interface

uses
  SysUtils;

type
  // ����������� �����: dNone - ��� ����������� (�������������� ��������), dUpLeft..dDownRight -
  // �� �������: �����-�����, �����, �����-������, �����, ������, ����-�����, ����, ����-������.
  TDirection = (dNone, dUpLeft, dUp, dUpRight, dLeft, dRight, dDownLeft, dDown, dDownRight);

  // ����������.
  TCoord = record
    X: Integer;
    Y: Integer;
  public
    // ���������� ����� ��������� ���� �����.
    class operator Add(const A, B: TCoord): TCoord;
    // ���������� �������� ��������� ���� �����.
    class operator Subtract(const A, B: TCoord): TCoord;
    // ���������� ������������ ��������� ���� �����.
    class operator Multiply(const A, B: TCoord): TCoord; overload;
    // ���������� ������������ ����� A � ��������� ����� B.
    class operator Multiply(A: Integer; const B: TCoord): TCoord; overload;
  public
    // ���������� ����������� ��� ���������. ���� ��� ��������� ���
    // ����������� �����������, �� ����� ������������� ����������.
    function Direction: TDirection;
  end;

// �������� ���������.
function Coord(X, Y: Integer): TCoord; overload;
function Coord(Dir: TDirection): TCoord; overload;

implementation

const
  Xs: array[TDirection] of Integer = (0, -1, 0, 1, -1, 1, -1, 0, 1);
  Ys: array[TDirection] of Integer = (0, -1, -1, -1, 0, 0, 1, 1, 1);

//----------------------------------------------------------------------------------------------------------------------
function Coord(X, Y: Integer): TCoord;
begin
  Result.X := X;
  Result.Y := Y;
end;

//----------------------------------------------------------------------------------------------------------------------
function Coord(Dir: TDirection): TCoord;
begin
  Result.X := Xs[Dir];
  Result.Y := Ys[Dir];
end;

//----------------------------------------------------------------------------------------------------------------------
class operator TCoord.Add(const A, B: TCoord): TCoord;
begin
  Result.X := A.X + B.X;
  Result.Y := A.Y + B.Y;
end;

//----------------------------------------------------------------------------------------------------------------------
function TCoord.Direction: TDirection;
var
  I: TDirection;
begin
  // ���� ���������� �����������.
  for I := dNone to High(TDirection) do begin
    if (X = Xs[I]) and (Y = Ys[I]) then begin
      Result := I;
      Exit;
    end;
  end;
  // ���� ���������� ����������� �� �������, ���������� ����������.
  raise Exception.Create('������������ ����������');
end;

//----------------------------------------------------------------------------------------------------------------------
class operator TCoord.Multiply(const A, B: TCoord): TCoord;
begin
  Result.X := A.X * B.X;
  Result.Y := A.Y * B.Y;
end;

//----------------------------------------------------------------------------------------------------------------------
class operator TCoord.Multiply(A: Integer; const B: TCoord): TCoord;
begin
  Result.X := A * B.X;
  Result.Y := A * B.Y;
end;

//----------------------------------------------------------------------------------------------------------------------
class operator TCoord.Subtract(const A, B: TCoord): TCoord;
begin
  Result.X := A.X - B.X;
  Result.Y := A.Y - B.Y;
end;

end.
