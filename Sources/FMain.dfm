object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = #1050#1083#1077#1090#1086#1095#1085#1099#1081' '#1072#1074#1090#1086#1084#1072#1090
  ClientHeight = 540
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 477
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    784
    540)
  PixelsPerInch = 96
  TextHeight = 13
  object FieldGrid: TStringGrid
    Left = 8
    Top = 8
    Width = 504
    Height = 504
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultColWidth = 10
    DefaultRowHeight = 10
    DefaultDrawing = False
    FixedCols = 0
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goThumbTracking]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Visible = False
    OnDrawCell = FieldGridDrawCell
    OnSelectCell = FieldGridSelectCell
  end
  object GroupBox1: TGroupBox
    Left = 520
    Top = 2
    Width = 256
    Height = 150
    Anchors = [akTop, akRight]
    Caption = ' '#1055#1072#1088#1072#1084#1077#1090#1088#1099' '
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 16
      Width = 95
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088#1085#1086#1089#1090#1100' '#1087#1086#1083#1103':'
    end
    object Label2: TLabel
      Left = 10
      Top = 60
      Width = 72
      Height = 13
      Caption = #1063#1080#1089#1083#1086' '#1075#1088#1080#1073#1086#1074':'
    end
    object Label3: TLabel
      Left = 10
      Top = 104
      Width = 105
      Height = 13
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099' '#1079#1072#1089#1077#1074#1072':'
    end
    object FieldSizeEdit: TEdit
      Left = 10
      Top = 32
      Width = 105
      Height = 21
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 0
      Text = '100'
    end
    object CreateFieldButton: TButton
      Left = 130
      Top = 28
      Width = 113
      Height = 25
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1087#1086#1083#1077
      TabOrder = 1
      OnClick = CreateFieldButtonClick
    end
    object SeedCountEdit: TEdit
      Left = 10
      Top = 76
      Width = 105
      Height = 21
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 2
      Text = '1'
    end
    object SeedButton: TButton
      Left = 130
      Top = 72
      Width = 113
      Height = 25
      Caption = #1047#1072#1089#1077#1103#1090#1100' '#1075#1088#1080#1073#1099
      Enabled = False
      TabOrder = 3
      OnClick = SeedButtonClick
    end
    object SeedXEdit: TEdit
      Left = 10
      Top = 120
      Width = 47
      Height = 21
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 4
      Text = '15'
    end
    object SeedYEdit: TEdit
      Left = 68
      Top = 120
      Width = 47
      Height = 21
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 5
      Text = '15'
    end
    object SeedXYButton: TButton
      Left = 130
      Top = 116
      Width = 113
      Height = 25
      Caption = #1047#1072#1089#1077#1103#1090#1100' '#1075#1088#1080#1073
      Enabled = False
      TabOrder = 6
      OnClick = SeedXYButtonClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 520
    Top = 238
    Width = 256
    Height = 234
    Anchors = [akTop, akRight]
    Caption = ' '#1052#1086#1076#1077#1083#1080#1088#1086#1074#1072#1085#1080#1077' '
    TabOrder = 3
    object Label4: TLabel
      Left = 10
      Top = 16
      Width = 98
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1096#1072#1075#1086#1074':'
    end
    object Label5: TLabel
      Left = 10
      Top = 64
      Width = 77
      Height = 13
      Caption = #1052#1080#1085'. '#1088#1077#1089#1091#1088#1089#1086#1074':'
    end
    object Label6: TLabel
      Left = 141
      Top = 64
      Width = 82
      Height = 13
      Caption = #1052#1072#1082#1089'. '#1088#1077#1089#1091#1088#1089#1086#1074':'
    end
    object Label7: TLabel
      Left = 10
      Top = 112
      Width = 96
      Height = 13
      Caption = #1056#1072#1089#1093'. '#1088#1077#1089'. '#1085#1072' '#1088#1086#1089#1090':'
    end
    object Label8: TLabel
      Left = 141
      Top = 112
      Width = 104
      Height = 13
      Caption = #1056#1072#1089#1093'. '#1088#1077#1089'. '#1085#1072' '#1078#1080#1079#1085#1100':'
    end
    object StepCountEdit: TEdit
      Left = 10
      Top = 32
      Width = 105
      Height = 21
      Ctl3D = True
      MaxLength = 4
      NumbersOnly = True
      ParentCtl3D = False
      TabOrder = 0
      Text = '10'
    end
    object StartButton: TButton
      Left = 130
      Top = 28
      Width = 113
      Height = 25
      Caption = #1053#1072#1095#1072#1090#1100
      Enabled = False
      TabOrder = 1
      OnClick = StartButtonClick
    end
    object RemoveAllButton: TButton
      Left = 60
      Top = 200
      Width = 137
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1075#1088#1080#1073#1099
      Enabled = False
      TabOrder = 7
      OnClick = RemoveAllButtonClick
    end
    object JmpMemCheckBox: TCheckBox
      Left = 10
      Top = 160
      Width = 131
      Height = 17
      Caption = #1055#1072#1084#1103#1090#1100' '#1087#1088#1080' '#1087#1088#1099#1078#1082#1077
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
    object ResMinEdit: TEdit
      Left = 10
      Top = 80
      Width = 105
      Height = 21
      TabOrder = 2
      Text = '0.2'
    end
    object ResMaxEdit: TEdit
      Left = 141
      Top = 80
      Width = 105
      Height = 21
      TabOrder = 3
      Text = '0.6'
    end
    object GrowthConsumptionEdit: TEdit
      Left = 10
      Top = 128
      Width = 105
      Height = 21
      TabOrder = 4
      Text = '0.001'
    end
    object LivingConsumptionEdit: TEdit
      Left = 141
      Top = 128
      Width = 105
      Height = 21
      TabOrder = 5
      Text = '0.0001'
    end
    object NoTorCheckBox: TCheckBox
      Left = 10
      Top = 176
      Width = 131
      Height = 17
      Caption = #1054#1090#1082#1083#1102#1095#1080#1090#1100' '#1058#1054#1056
      TabOrder = 8
    end
  end
  object GroupBox3: TGroupBox
    Left = 520
    Top = 158
    Width = 256
    Height = 74
    Anchors = [akTop, akRight]
    Caption = ' '#1047#1072#1075#1088#1091#1079#1082#1072' '#1088#1077#1089#1091#1088#1089#1086#1074' '
    TabOrder = 2
    DesignSize = (
      256
      74)
    object OpenResButton: TButton
      Left = 60
      Top = 40
      Width = 137
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
      Enabled = False
      TabOrder = 1
      OnClick = OpenResButtonClick
    end
    object NormalizeCheckBox: TCheckBox
      Left = 10
      Top = 16
      Width = 153
      Height = 17
      Caption = #1053#1086#1088#1084#1072#1083#1080#1079#1086#1074#1072#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 520
    Width = 784
    Height = 20
    Panels = <
      item
        Text = #1057#1086#1079#1076#1072#1081#1090#1077' '#1087#1086#1083#1077' '#1076#1083#1103' '#1085#1072#1095#1072#1083#1072' '#1088#1072#1073#1086#1090#1099
        Width = 320
      end
      item
        Text = #1056#1072#1079#1084#1077#1088' '#1087#1086#1083#1103': '#1085#1077' '#1079#1072#1076#1072#1085
        Width = 160
      end
      item
        Width = 50
      end>
  end
  object OpenResDialog: TOpenDialog
    Filter = 'Quasy-natural file (*.qnl)|*.qnl|Unnormized file (*.csv)|*.csv'
    Left = 456
    Top = 16
  end
end
